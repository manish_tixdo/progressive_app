/**
 * Created by consultadd on 20/10/16.
 */
import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {platformUniversalDynamic, UniversalModule} from "angular2-universal/typings";
import {AppComponent} from "./app.component";
import {AppModule} from "./app.module";
import {AppShellModule} from '@angular/app-shell';

@NgModule({
  bootstrap: [AppComponent],
  imports: [
    AppModule,
    AppShellModule.prerender(),
    UniversalModule.withConfig({
        preboot: true // enables event replay
}),]
})


export class ShellModule {}
platformUniversalDynamic()
  .serializeModule(ShellModule)
  .then(html => console.log(html));
