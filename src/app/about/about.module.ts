/**
 * Created by consultadd on 20/10/16.
 */

import	{	NgModule	}	from	'@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../shared/shared.module";
import {AboutComponent} from "./about.component";



const routerConfig= [
  { path: '', component: AboutComponent },
]

@NgModule({
  imports:	[
    RouterModule.forChild(routerConfig),
  ],

  declarations:	[AboutComponent],

  providers:	[],
  exports:	[AboutComponent]
})
export	class	AboutModule	{}
